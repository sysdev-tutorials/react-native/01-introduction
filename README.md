# Part I: Introduction

In this section, first we will learn about the following things

 - What is cross-platform development?
 - What is react-native?

 Then we will set up react-native development environment, and create and run our very first mobile app. Yey!!


## What is cross-platform development
It is a modern application development approach where applications are created or developed in such a way that that are compatible with multiple operating systems.

In the context of mobile applications what it means is, developers can use one programming language and one source code repo to develop mobile app that will function on multiple mobile platforms such as Android, iOS and so on.

## Why cross-platform development
Basically, it is easier for programmers to develop one cross-platform app using one codebase and one programming language compared to developing multiple apps using multiple programming languages and multiple set of development tools. That also means reduced cost and development time and possibly shorter time-to-market!​


## What is react-native then?
It is a cross-platform mobile application development tool. What does that mean is, it allows developers to develop a mobile app in a JavaScript style programming language called react-native, and the app will run on both Android and iOS.

It is developed and maintained by Facebook. More on their official website https://reactnative.dev/

## Why react-native, are there alternatives?
Besides react-native, there are couple of tools and technologies that developers can use for cross-platform mobile app development. Some names to mention are Xamarin and Flutter.

React-native is one of the most popular development framework out there. This is the reason why this this chosen in this work. This does not mean that other cross-platform frameworks are bad! Do you own research for comparision between them. Some intersting reading for react-native and its alternatives are following.

https://doit.software/blog/flutter-vs-react-native#screen20

https://blog.logrocket.com/react-native-vs-flutter/#:~:text=Flutter%20uses%20a%20widget%20style,to%20worry%20about%20compatibility%20issues



# Part II: Set up development environment for react-native

## Prerequisite

Lets first vist react-native getting started website and try out an interactive sample!

- Visit https://reactnative.dev/docs/getting-started
- Run interactive sample to get some idea how the app development works, for example change the source code in the interactive sample in the browser, and reload the app in your target device. Changes should be reflected. Yey!!
  - Try runing on web by selecting "web" tab at the buttom
  - Try either on Android or iOS. You might need to wait a while until a device is available for you
  - Try running on your mobile device. For this you need to download Expo Go app on your device. When you downloaded the Expo Go app, scan the QR code presented in the above link.

Congratulations for your (probably) first mobile app!! We will gradually learn what is happening under the hood, and also learn proper way of writing and changing code for your app. So, hang on :)

## Setup local development environment
Note: Later in the course, we will use react-native CLI. However for quick start we will begin with Expo CLI, which is a tool built around react-native.

Note: The commands used in the following section are tesed on Mac OS only, so they do not work in your OS, you will have to find the proper formal of those commands!

For setting up your local development environment, follow this link: https://reactnative.dev/docs/environment-setup. 

In the following sections, only summary is provided. Users are recommended to use official documentation mentioned above!

## React-native setup

Lets start with Expo CLI first. Then we will continue with react-native CLI.

### Setup and use Expo CLI

#### Install Expo CLI
- Make sure that you have node version 14 or higher. Check with this command in termimal ```node -v```
- Make sure that you have npm installed in your machine, Check with this command in terminal ```npm -v```
- Install expo CLI, for example with this command, ```npm install -g expo-cli```. This will install expo CLI scoped global to your system. Make sure that expo is correctly installed in your system with the command ```expo -V```
- (It is assumed that you have already installed Expo Go app in your device, as described in the prerequisite section above)

#### Create react-native project with expo CLI
- Create a react-native project with this command ```expo init```. Select ```blank``` option if you are asked.
- Then execute the command ```npm start``` on terminal. This command will start a development server in your machine. A QR code will be presented in the terminal, that can be scanned by your mobile device (Expo Go app on Android or Camera app on iOS). Then behind the schene, expo will download and update your app on your device. 
- Try changing the application code (for example <Text> component in App.js file under your project directory). When you save your changes, application should reload automatically.

Congratulations, you have created and modified your react-native app and installed (dev verion of it) on your mobile device!

Note: When you do ```npm start``` it starts a 'Metro bundler', what dos that do? - Well its a development platform for  Reactive Native. It acts as a JavaScript buldler, manages project assets, caches builds and performs hot reloading. It basically improves developer experience.

### Should I continue to use Expo?
As mentioned above, Expo offers tools and services built for React Native​ thereby simplifying the creation and testing of React Native apps. Note however that React Native apps can be built without Expo too i.e. using plain react-native tools. So the question is when and why should Expo be used?​

The answer depends on you - the developer and the situation you have! 

If you are absolute beginner and just want to started with React Native fast and dirty, Expo is for you. With Expo, no prior knowledge of native coding is needed - for example Java, Objective C, Swift and so on. Expo handles all the native code under the hood, and this is not available to the developers. 

Whereas, if you build React Native apps from scratch without Expo, you will need to use a tiny bit of your native mobile coding skills to tweak some native code on both platforms. For anyone who wants to avoid getting into that layer, Expo is the best fit.

With Expo, no local development environment setup is needed such as Android Studio or Xcode installations and configurations. App installation and update done via publish over the air (OTA) model. 

Expo is free and open-source.

### So, when should I use react-native CLI then?​

The answer depends on you - the developer and the situation you have! 

If you are (or want to be) a professional react-native developer then you probably would like to use react-native CLI directly. Eventhough Expo offers greate features via CLI and SDK, one important feature that is missing is the ability to work with Native modules - for example integration with own native modules (code written in Native language) or third party native modules.

However remember that it can be more time consuming as it requires loca environmental setup such as Android Studio and Xcode, that might have different configurations in different operating systems. Besides that if you want to work your physical devices, you will have to connect your phone to your pc via USB cable! 

### Setup and use react-native CLI 
(Warning - Note that set up of React Native depends on the type of the operating system in your development machine and underlying hardware architecture, and target Mobile operating system. The instructions given in the following section are tested only in Mac machine with Apple M1 chip. For your machine, the instructions might be different. So, please follow official guide from React Native for environmental setup - https://reactnative.dev/docs/environment-setup)

### Setup react-native CLI – Mac/Android​

As mentioned above, you are strongly recommendted to follow official documentation here https://reactnative.dev/docs/environment-setup​

Summary of react-native setup and is on Mac for Android platform is below.
 - Make sure you have installed ```node```, ```watchman``` and ```Java``` 11 or higher in your system​
   - install node using ```brew install node```
   - install watchman using ```brew install watchman```
   - install java using these commands ```brew tap homebrew/cask-versions``` and ```brew install --cask zulu11```. If you previously have Java 11 or higher, ot should probably just work fine. Note however that the Zulu OpenJDK distribution offers JDKs for both Intel and M1 Macs. This will make sure your builds are faster on M1 Macs compared to using an Intel-based JDK.
- Install and configure Android Studio, Android SDK, set up ANDROID_SDK_ROOT environment variable, set up AVD or physical devices. Please follow official documentation. During installation note the following tips!
  - When you open downloaded Android Studio for the first time, open it with right click -> open​
  - Select 'Install Type' as 'Custom', and follow instructions on official documentation, especially on which items to install. When installation is complete properly configure correct version of Android SDK within the SDK Manager and SDK tools. Also create/configure Android virtual device (AVD) with corresponding settings for example ```Android 12 (S)``` and ```API 31​```

Note: If you previously installed a global react-native-cli package, please remove it as it may cause unexpected issues. With npx react-native <command>, the current stable version of the CLI will be downloaded and executed at the time the command is run. Some useful npx commands are following ​

 - to create a react-native project, ```npx react-native init <project_name>```​
 - to start a metro bundler run this command from project folder, ```npx react-native start​```
 - to start your project run this command from project folder, ```npx react-native run-android​```. When this command is run, it will fireup an Android emulator (if not already running) and install the React Native app on the android emulator device. 

 Note: If asked about CocoaPods installation, select the option with 'gem'. For M1 arch machines, use following commands if there arise issues installing CocoaPods, ```sudo arch -x86_64 gem install ffi``` and ```arch -x86_64 pod install```

Congratulations, you should be now able to create mobile app using React Native CLI and install it on Andriod emulator device! 

### Setup react-native CLI – Mac/iOS
As mentioned above, you are strongly recommendted to follow official documentation here https://reactnative.dev/docs/environment-setup​

Summary of react-native setup and is on Mac for iOS platform is below.
 - Make sure you have installed ```node``` and ```watchman``` in your system​
   - install node using ```brew install node```
   - install watchman using ```brew install watchman```
 - Install Xcode is via the Mac App Store. After installing Xcode, install  all the necessary tools and iOS Simulator(s) to build and run your iOS app. If you have already installed Xcode on your system, make sure it is version 10 or newer.​ Note that installing Xcode, Xcode Command Line Tools and iOS simulators takes time, so be patient!
 - Install CocoaPods (a dependency manager for iOS apps) using this command ```sudo gem install cocoapods```. ​Note that for M1 chip based machines, use following additional commands if there arise issues installing CocoaPods, ```sudo arch -x86_64 gem install ffi``` and ```arch -x86_64 pod install```

Note: If you previously installed a global react-native-cli package, please remove it as it may cause unexpected issues. With ```npx react-native <project_name>```, the current stable version of the rect-native CLI will be downloaded and executed at the time the command is run. Some useful npx commands are following ​

 - to create a react-native project, ```npx react-native init <project_name>```​
   - run this command 'cd ios && pod install' from project home folder. This will install ios dependencies
 - to start a metro bundler run this command from project folder, ```npx react-native start​```
 - to start your project run this command from project folder, ```npx react-native run-ios```. When this command is run, it will fireup an iOS emulator (if not already running) and install the React Native app on the iOS emulator device.

​Congratulations, you should be now able to create mobile app using React Native CLI and install it on iOS emulator device! 

Thats it for this section! 

